import { createContext } from "react"

const AddonsContext = createContext({
  addon: null,
  addonData: null,
  setContext() { },
});

export default AddonsContext;