import React, { useContext, useEffect, useState } from "react";
import { Link, NavLink } from "react-router-dom";
import { Col, Container, Row } from "reactstrap";
import AppContext from "../../providers/AppContext";
import {
  fromUsersDocument,
  getLiveUsers,
  userBlocking,
  usersRole,
} from "../../services/users-service";
import { Addons } from "../Addons/Addons";
import { Rating } from "../Rating/Rating";

export const AdminDashboard = () => {
  const [users, setUsers] = useState([]);
  const { userData } = useContext(AppContext);

  const [filteredUsers, setFilteredUsers] = useState([]);

  useEffect(() => {
    getLiveUsers((snapshot) => {
      setUsers(fromUsersDocument(snapshot));
      setFilteredUsers(fromUsersDocument(snapshot));
    });
  }, []);

  const handleBlockUser = (username, role) => {
    if (role !== usersRole.BLOCKED) {
      userBlocking(username, usersRole.BLOCKED);
    } else {
      userBlocking(username, usersRole.BASIC);
    }
  };

  const handleSearch = (e) => {
    const searchUser = e.target.value;
    setFilteredUsers(
      users.filter(
        (user) =>
          user.username.includes(searchUser) || user.email.includes(searchUser)
      )
    );
  };

  return (
    <section>
      {userData?.role === usersRole.ADMIN && (
        <Container>
          <Row>
            <span className="input">
              <br></br>
              <input
                type="text"
                placeholder="Search ..."
                onChange={(event) => handleSearch(event)}
              ></input>
            </span>
            <Col sm="4" className="mb-5">
              {filteredUsers &&
                filteredUsers.map((user, key) => (
                  <div className="profile__details" key={key}>
                    <div className="profile__avatar align-item-center">
                      {user.avatarUrl ? (
                        <img
                          className="w-100 profile__avatar"
                          src={user.avatarUrl}
                          alt="profile"
                        />
                      ) : (
                        <img
                          className="profile__avatar w-100 align-item-center"
                          alt="default_avatar"
                        />
                      )}
                    </div>
                    <div style={{ color: "white" }}>
                      <NavLink
                        style={{ color: "white" }}
                        to="/admin-board-profile"
                        state={{
                          username: user.username,
                          avatarUrl: user.avatarUrl,
                          email: user.email,
                          addons: user.addons,
                        }}
                      >
                        <h5>@{user.username}</h5>
                      </NavLink>
                      <span>email: {user.email}</span>
                      <br />
                      <span>
                        {user.addons
                          ? `Your plugins are: ${user.addons.length}`
                          : "Create your first plugin "}
                      </span>
                      <br />
                      <span className="edit__span">
                        <Link to="/edit-profile">Edit profile</Link>
                      </span>
                      <span className="block_button">
                        <button
                          onClick={() =>
                            handleBlockUser(user.username, user.role)
                          }
                        >
                          {user.role === usersRole.BLOCKED
                            ? "Unblock"
                            : "Block"}
                        </button>
                      </span>
                      <br />
                      <Rating user={user} />
                    </div>
                  </div>
                ))}
            </Col>

            <Col lg="3" sm="8">
              <div>
                <Addons status={1} />
              </div>
            </Col>
          </Row>
        </Container>
      )}
    </section>
  );
};
