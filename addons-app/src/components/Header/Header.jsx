import React from "react";
import AppContext from "../../providers/AppContext";
import { useContext } from "react";
import "./header.css";
import { Container } from "reactstrap";
import { NavLink, Link } from "react-router-dom";
import { AvatarDropdown } from "../AvatarDropdown/AvatarDropdown";
import { usersRole } from "../../services/users-service";

const NAV_LINKS = [
  {
    display: "Home",
    url: "/home",
  },

  {
    display: "Market",
    url: "/market",
  },
  {
    display: "Contact",
    url: "/contact",
  },
  {
    display: "About",
    url: "/about",
  },
];
export const Header = () => {
  const appState = useContext(AppContext);
  const { userData } = useContext(AppContext);

  return (
    <header className="header">
      <Container>
        <div className="navigation">
          <div className="logo">
            <h2 className="d-flex gap-2 align-items-center">
              <span>
                <i className="ri-fire-fill"></i>
              </span>{" "}
              Shook{" "}
            </h2>
          </div>
          <div className="nav__menu">
            <ul className="nav__list">
              {NAV_LINKS.map((item, index) => (
                <li className="nav__item" key={index}>
                  <NavLink
                    to={item.url}
                    className={(navClass) =>
                      navClass.isActive ? "active" : ""
                    }
                  >
                    {item.display}
                  </NavLink>
                </li>
              ))}
            </ul>
          </div>
          {appState.user === null ? (
            <div className="nav__right d-flex align-items-center gap-2">
              <button className="btn d-flex gap-2 align-items-center">
                <span>
                  <i className="ri-login-circle-line"></i>
                </span>
                <Link to="/sign-in">Sign In</Link>
              </button>

              <button className="btn d-flex gap-2 align-items-center">
                <span>
                  <i className="ri-contacts-book-2-line"></i>
                </span>
                <Link to="/sign-up">Sign Up</Link>
              </button>
              <span className="mobile__menu">
                <i className="ri-menu-line"></i>
              </span>
            </div>
          ) : userData.role === usersRole.BLOCKED ? (
            <AvatarDropdown />
          ) : (
            <div className="nav__right d-flex align-items-center gap-2">
              <button className="btn d-flex gap-2 align-items-center">
                <span>
                  <i className="ri-file-code-line"></i>
                </span>
                <Link to="/create">Create</Link>
              </button>
              <AvatarDropdown />
              <span className="mobile__menu">
                <i className="ri-menu-line"></i>
              </span>
            </div>
          )}
        </div>
      </Container>
    </header>
  );
};
