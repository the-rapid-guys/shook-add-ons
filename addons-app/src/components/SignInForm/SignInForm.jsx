import React from 'react' 
import {useState, useContext} from 'react'
import AppContext from '../../providers/AppContext';
import { getUserData } from '../../services/users-service';
import { loginUser } from '../../services/authentication';
import { useNavigate } from 'react-router-dom';
import { Link } from 'react-router-dom';
import {Container, Row, Col} from 'reactstrap'
import './sign_in.css'
export const SignInForm = () => {
  const [form, setForm] = useState({
    email: '',
    password: '',
  });

  const { setContext } = useContext(AppContext);
  const navigate = useNavigate();

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const login = (e) => {
    e.preventDefault();

    loginUser(form.email, form.password)
      .then((u) => {
        return getUserData(u.user.uid).then(
          
          (snapshot) => {
          if (snapshot.exists()) {
            setContext({
              user: u.user,
              userData: snapshot.val()[Object.keys(snapshot.val())[0]],
            });

            navigate('/home');
          }
        });
      })
      .catch(console.error);
  };
  
  return (
      <section>
          <Container>
              <Row>
                  <Col lg='1' className=''>
      <div className='signin__wrapper'>
    
<div className="login-box ">
  <h2>Sign In</h2>
  <form>
    <div className="user-box">
      <input type="email" value={form.email} onChange={updateForm('email')}/>
      <label>Email</label>
    </div>
    <div className="user-box">
      <input type="password" value={form.password} onChange={updateForm('password')}/>
      <label>Password</label>
    </div>
    <button type='submit' onClick={login}>
        <span></span>
      Sign In
    </button>
  </form><br/>
  <p className='d-flex align-items-center justify-content-between'>
                                        Don't have an account? <Link to="/sign-up" ><span>Sign up</span></Link>.
                                    </p>
</div>
</div>
</Col>
</Row>
</Container> 
      </section>                              
                               
  )
}
