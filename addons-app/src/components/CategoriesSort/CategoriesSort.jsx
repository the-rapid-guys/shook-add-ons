import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import './categories_sort.css';
import { useEffect, useState } from 'react';
import {
  getLiveAddons,
  fromAddonsDocument,
} from '../../services/addon-service';
import { AddonCard } from '../ui/AddonCard/AddonCard';

export const CategoriesSort = () => {
  const [addons, setAddons] = useState([]);
  const [sortBy, setSortBy] = useState('all');
  const [filteredAddons, setFilteredAddons] = useState([]);
  const [count, setCount] = useState(8);

  useEffect(() => {
    const unsubscribe = getLiveAddons((snapshot) => {
      setAddons(fromAddonsDocument(snapshot).filter((el) => el.status === 2));
    });

    return () => unsubscribe();
  }, []);

  useEffect(() => {
    if (sortBy === 'coding') {
      setFilteredAddons(() =>
        [...addons].filter((el) => el.category === 'coding')
      );
    }
    if (sortBy === 'ui/ux') {
      setFilteredAddons(() =>
        [...addons].filter((el) => el.category === 'ui/ux')
      );
    }
    if (sortBy === 'web service') {
      setFilteredAddons(() =>
        [...addons].filter((el) => el.category === 'web service')
      );
    }
    if (sortBy === 'development') {
      setFilteredAddons(() =>
        [...addons].filter((el) => el.category === 'development')
      );
    }
    if (sortBy === 'database') {
      setFilteredAddons(() =>
        [...addons].filter((el) => el.category === 'database')
      );
    }
    if (sortBy === 'all') {
      setFilteredAddons(() => [...addons]);
    }

    if (sortBy === 'author') {
      setFilteredAddons(() =>
        [...addons].sort((a, b) =>
          a.author < b.author ? 1 : b.author < a.author ? -1 : 0
        )
      );
    }

    if (sortBy === 'rating') {
      setFilteredAddons(() =>
        [...addons].sort((a, b) =>
          a.rating < b.rating ? 1 : a.rating > b.rating ? -1 : 0
        )
      );
    }

    if (sortBy === 'downloads') {
      setFilteredAddons(() =>
        [...addons].sort((a, b) =>
          a.downloads < b.downloads ? 1 : a.downloads > b.downloads ? -1 : 0
        )
      );
    }
  }, [sortBy, addons]);

  const handleDropdownChange = (event) => {
    setSortBy(event.target.value);
  };

  return (
    <section>
      <Container>
        <Row>
          <Col lg="12" className="mb-5">
            <div className="market__addon__filter d-flex align-item-center justify-content-between">
              <div className="filter__left d-flex align-item-center gap-5">
                <div className="main__filter d-flex align-item-center"></div>
              </div>
              <div className="category d-flex align-items-center gap-3">
                <button className="btn__all" onClick={() => setSortBy('all')}>
                  <span>All</span>
                </button>
                <button className="btn" onClick={() => setSortBy('coding')}>
                  <span>Coding</span>
                </button>
                <button className="btn" onClick={() => setSortBy('ui/ux')}>
                  <span>UI/UX</span>
                </button>
                <button
                  className="btn"
                  onClick={() => setSortBy('web service')}
                >
                  <span>Web Service</span>
                </button>
                <button
                  className="btn"
                  onClick={() => setSortBy('development')}
                >
                  <span>Development</span>
                </button>
                <button className="btn" onClick={() => setSortBy('database')}>
                  <span>Database</span>
                </button>
              </div>

              <div className="filter__right d-flex align-item-center">
                <select onChange={handleDropdownChange}>
                  <option value="sort_by">Sort by</option>
                  <option value="author" onClick={() => setSortBy('author')}>
                    Author
                  </option>
                  <option
                    value="downloads"
                    onClick={() => setSortBy('downloads')}
                  >
                    Downloads
                  </option>
                  <option value="rating" onClick={() => setSortBy('rating')}>
                    Rating
                  </option>
                  <option value="commit">Last commit</option>
                </select>
              </div>
            </div>
          </Col>
        </Row>
        <Row>
          {filteredAddons.length === 0 ? (
            <div>No plug-ins</div>
          ) : (
            filteredAddons.slice(0, count).map((addon) => {
              return <AddonCard key={addon.id} addon={addon} />;
            })
          )}
          {filteredAddons.length !== 0 && count < filteredAddons.length && (
            <div
              className=""
              onClick={() => {
                setCount(() => filteredAddons.length);
              }}
            >
              <span
                style={{
                  color: 'white',
                  textDecoration: 'underline',
                  cursor: 'pointer',
                }}
              >
                View more
              </span>
            </div>
          )}
          {filteredAddons.length !== 0 && count >= filteredAddons.length && (
            <div
              className=""
              onClick={() => {
                setCount(() => 8);
              }}
            >
              <span
                style={{
                  color: 'white',
                  textDecoration: 'underline',
                  cursor: 'pointer',
                }}
              >
                View less
              </span>
            </div>
          )}
        </Row>
      </Container>
    </section>
  );
};
