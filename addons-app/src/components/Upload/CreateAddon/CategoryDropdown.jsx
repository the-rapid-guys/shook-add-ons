import React from "react";

export const CategoryDropdown = ({ category, setCategory }) => {
  const handleCategoryChange = (event) => {
    setCategory(event);
  };

  return (
    <div>
      <Dropdown
        options={[
          { label: "Category", value: "" },
          { label: "Coding", value: "coding" },
          { label: "Web service", value: "web service" },
          { label: "UI/UX", value: "ui/ux" },
          { label: "Database", value: "database" },
          { label: "Development", value: "development" },
        ]}
        value={category}
        onChange={handleCategoryChange}
      />
    </div>
  );
};
const Dropdown = ({ label, value, options, onChange }) => {
  return (
    <div>
      <label className="select">
        {label}
        <select value={value} onChange={onChange}>
          {options.map((option) => (
            <option value={option.value}>{option.label}</option>
          ))}
        </select>
      </label>
    </div>
  );
};
