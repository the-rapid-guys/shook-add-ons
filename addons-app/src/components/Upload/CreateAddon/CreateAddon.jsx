import * as React from "react";
import "./create_addon.css";
import {
  ref as storageRef,
  getDownloadURL,
  uploadBytes,
} from "firebase/storage";
import { storage } from "../../../firebaseConfig/firebase-config";
import { useState, useContext } from "react";
import AppContext from "../../../providers/AppContext";
import { Container, Row, Col } from "reactstrap";
import {
  createAddon,
  updateAddonDownloadLink,
  updateAddonDownloadLinkLogo,
} from "../../../services/addon-service";
import { usersRole } from "../../../services/users-service";
// import { useNavigate } from "react-router-dom";
import { CategoryDropdown } from "./CategoryDropdown";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export const CreateAddon = () => {
  const { userData } = useContext(AppContext);
  // const { user, userData } = useContext(AppContext);
  const [open, setOpen] = useState(false);

  const [form, setForm] = useState({
    title: "",
    tags: "",
    originURL: "",
    category: "",
    logo: "",
    content: "",
  });

  const handleClick = () => {
    setOpen((prev) => !prev);
  };

  const handleChange = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const fileUpload = (e) => {
    e.preventDefault(e);

    const fileLogo = e.target[4]?.files?.[0];
    const file = e.target[5]?.files?.[0];

    if (!fileLogo) return alert("Select plug-in logo");
    if (!file) return alert("Select file");

    createAddon(
      form.title,
      form.originURL,
      form.category,
      form.tags,
      userData.username,
      fileLogo,
      file,
      userData.avatarUrl
    )
      .then((res) => {
        const plugInLogo = storageRef(
          storage,
          `logos/${res.id}/${fileLogo.name}`
        );
        const plugInFile = storageRef(storage, `files/${res.id}/${file.name}`);

        uploadBytes(plugInLogo, fileLogo).then((snapshot) => {
          return getDownloadURL(snapshot.ref).then((url) => {
            updateAddonDownloadLinkLogo(res.id, url);
          });
        });

        uploadBytes(plugInFile, file).then((snapshot) => {
          return getDownloadURL(snapshot.ref).then((url) => {
            updateAddonDownloadLink(res.id, url);
          });
        });
      })
      .then((res) => {
        handleClick();
        form.title = "";
        form.tags = "";
        form.originURL = "";
        form.category = "";
        form.logo = "";
        form.content = "";
      });
  };
  return (
    <section>
      <Container>
        <Row>
          {userData.role !== usersRole.BLOCKED ? (
            <Col lg="12" className="mb-5">
              <div className="upload__wrapper">
                <div className="upload__box ">
                  <h2>Create Plugin</h2>
                  <form onSubmit={fileUpload}>
                    <div className="user__box">
                      <input
                        type="text"
                        name="title"
                        value={form.title}
                        onChange={handleChange("title")}
                      />
                      <label>Title</label>
                    </div>
                    <div className="user__box">
                      <input
                        placeholder="add your tags here separated by #"
                        type="text"
                        name="tags"
                        value={form.tags}
                        onChange={handleChange("tags")}
                      />
                      <label>Tags</label>
                    </div>
                    <div className="user__box">
                      <input
                        type="url"
                        name="originURL"
                        value={form.originURL}
                        onChange={handleChange("originURL")}
                      />
                      <label>URL GitHub</label>
                    </div>
                    <CategoryDropdown
                      category={form.category}
                      setCategory={handleChange("category")}
                    />

                    <div className="submitForm">
                      <input
                        type="file"
                        name="logo"
                        value={form.logo}
                        onChange={handleChange("logo")}
                      ></input>
                      <label>Add logo</label>
                      <input
                        type="file"
                        name="content"
                        value={form.content}
                        onChange={handleChange("content")}
                      ></input>
                      <label>Add file</label>
                      <button type="submit">Submit</button>
                      <Snackbar
                        open={open}
                        autoHideDuration={6000}
                        onClose={handleClick}
                      >
                        <Alert
                          onClose={handleClick}
                          severity="success"
                          sx={{ width: "100%" }}
                        >
                          Plug-in added successfully!
                        </Alert>
                      </Snackbar>
                    </div>
                  </form>
                  <br />
                </div>
              </div>
            </Col>
          ) : (
            <p>
              You don't have permission to create addon as user has been
              blocked.
            </p>
          )}
        </Row>
      </Container>
    </section>
  );
};
