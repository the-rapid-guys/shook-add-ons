import { ref } from "firebase/storage";
import {
  ref as storageRef,
  getDownloadURL,
  uploadBytes,
} from "firebase/storage";
import { storage } from "../../../firebaseConfig/firebase-config";
import { updateUserProfilePicture } from "../../../services/users-service";
import { useContext } from "react";
import AppContext from "../../../providers/AppContext";
import { Container, Row, Col } from "reactstrap";
import "./upload_avatar.css";

export const UploadAvatar = () => {
  const { user, userData, setContext } = useContext(AppContext);
  const userProfile = userData.username;

  const avatarUpload = (e) => {
    e.preventDefault(e);

    const file = e.target[0]?.files?.[0];

    if (!file) return alert("Select file");

    const picture = storageRef(storage, `images/${userProfile}/files`);

    uploadBytes(picture, file)
      .then((snapshot) => {
        return getDownloadURL(snapshot.ref).then((url) => {
          return updateUserProfilePicture(userProfile, url).then(() => {
            setContext({
              user,
              userData: {
                ...userData,
                fileUrl: url,
              },
            });
          });
        });
      })
      .catch(console.error);
  };
  console.log(user);
  return (
    <section>
      <Container>
        <Row>
          <Col lg="12" className="mb-5">
            <div className="upload__wrapper">
              <div className="upload__box ">
                <h2>Create Plugin</h2>
                <form>
                  {/* <div className="user__box">
                  <input type="text" />
                  <label>Name</label>
                </div> */}
                  <div className="user__box">
                    <input type="text" />
                    <label>Title</label>
                  </div>
                  <div className="user__box">
                    <input type="text" />
                    <label>Category</label>
                  </div>
                  <div className="user__box">
                    <input type="text" />
                    <label>Tags</label>
                  </div>
                  <div className="user__box">
                    <input type="text" />
                    <label>URL GitHub</label>
                  </div>
                  <div className="user__box">
                    <input type="text" />
                    <label>Description</label>
                  </div>

                  {/* <span></span> */}
                </form>

                <div>
                  {userData.fileUrl ? (
                    <img src={userData.fileUrl} alt="profile" />
                  ) : null}
                  <div className="submitForm">
                    <form onSubmit={avatarUpload}>
                      <input type="file" name="image"></input>
                      <button type="submit">Submit</button>
                    </form>
                  </div>
                </div>

                <br />
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};
