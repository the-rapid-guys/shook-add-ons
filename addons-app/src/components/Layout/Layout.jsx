import React from "react";
import { Routers } from "../../routes/Routers";
import { Header } from "../Header/Header";
import { Footer } from "../Footer/Footer";
import AppContext from "../../providers/AppContext";
import { HashRouter } from "react-router-dom";
import { useState } from "react";
// import { getUserRepo, getLastCommit } from '../../services/github-service'
import { getUserData } from "../../services/users-service";
import { useEffect } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth } from "../../firebaseConfig/firebase-config";
export const Layout = () => {
  const [appState, setAppState] = useState({
    user: null,
    userData: null,
  });

  // let [user, loading, error] = useAuthState(auth);
  let [user] = useAuthState(auth);
  useEffect(() => {
    if (user === null) return;

    getUserData(user.uid)
      // console.log(user.uid)
      .then((snapshot) => {
        if (!snapshot.exists()) {
          throw new Error("Something went wrong!");
        }

        setAppState({
          user,
          userData: snapshot.val()[Object.keys(snapshot.val())[0]],
        });
      })
      .catch((e) => alert(e.message));
  }, [user]);

  return (
    <HashRouter basename="/">
      <AppContext.Provider value={{ ...appState, setContext: setAppState }}>
        <div>
          <Header />
        </div>
        <div>
          <Routers />
        </div>
        <Footer />
      </AppContext.Provider>
    </HashRouter>
  );
};
