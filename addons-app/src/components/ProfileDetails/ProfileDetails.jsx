import React, { useEffect, useState } from "react";
import "./profile_details.css";
import { Container, Row, Col } from "reactstrap";
import AppContext from "../../providers/AppContext";
import { useContext } from "react";
import defaultAvatar from "../../assets/images/default_pic.png";
import { Link } from "react-router-dom";
import { Rating } from "../Rating/Rating";
import { AddonCard } from "../ui/AddonCard/AddonCard";
import {
  fromAddonsDocument,
  getLiveAddons,
} from "../../services/addon-service";

export const ProfileDetails = () => {
  // const { user, userData } = useContext(AppContext);
  const { userData } = useContext(AppContext);
  const [addons, setAddons] = useState([]);

  useEffect(() => {
    const unsubscribe = () => {
      getLiveAddons((snapshot) => setAddons(fromAddonsDocument(snapshot)));
    };
    return () => unsubscribe();
  }, []);

  return (
    <section>
      {userData && (
        <Container>
          <Row>
            <Col sm="4" className="mb-5">
              <div className="profile__details gap-10 justify-content-between">
                <div className="profile__avatar align-item-center">
                  {userData.avatarUrl ? (
                    <img
                      className="w-100 profile__avatar"
                      src={userData.avatarUrl}
                      alt="profile"
                    />
                  ) : (
                    <img
                      className="profile__avatar w-100 align-item-center"
                      src={defaultAvatar}
                      alt="default_avatar"
                    />
                  )}
                </div>
                <div className="info__card">
                  <h5>@{userData.username}</h5>
                  <span>email: {userData.email}</span>
                  <br />
                  <h6>Phone: {userData.phoneNumber}</h6>

                  <h6>
                    {userData?.addons
                      ? `Your plugins are: ${userData.addons.length}`
                      : "Create your first plugin"}
                  </h6>

                  <span className="edit__span">
                    <Link to="/edit-profile">Edit profile</Link>
                  </span>

                  <Rating user={userData} />
                  <br />
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            {addons
              .filter((addon) => addon.author === userData.username)
              .map((addon) => {
                return <AddonCard key={addon.id} addon={addon} />;
              })}
          </Row>
        </Container>
      )}
    </section>
  );
};
