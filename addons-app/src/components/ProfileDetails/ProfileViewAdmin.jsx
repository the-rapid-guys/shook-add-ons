import React from "react";
import "./profile_details.css";
import { Container, Row, Col } from "reactstrap";
import defaultAvatar from "../../assets/images/default_pic.png";
import { Link, useLocation } from "react-router-dom";
import { BannerSection } from "../ui/BannerSection/BannerSection";
export const ProfileViewAdmin = (username, avatarUrl, email, addons) => {
  // console.log("ako");
  const location = useLocation();
  // console.log(location.state.username);
  return (
    <>
      <BannerSection />
      <section>
        <Container>
          <Row>
            <Col sm="4" className="mb-5">
              <div className="profile__details">
                <div className="profile__avatar align-item-center">
                  {location.state.avatarUrl ? (
                    <img
                      className="w-100 profile__avatar"
                      src={location.state.avatarUrl}
                      alt="profile"
                    />
                  ) : (
                    <img
                      className="profile__avatar w-100 align-item-center"
                      src={defaultAvatar}
                      alt="default_avatar"
                    />
                  )}
                </div>
                <div className="info__card">
                  <h5>@{location.state.username}</h5>
                  <span>email: {location.state.email}</span>

                  <br />
                  <span>
                    {location.state.addons
                      ? `Your plugins are: ${location.state.addons.length}`
                      : "Create your first plugin "}
                    <Link to="/create">here.</Link>
                  </span>
                  <br />
                  <span className="edit__span">
                    <Link to="/edit-profile">Edit profile</Link>
                  </span>
                  <br />
                </div>
              </div>
            </Col>

            <Col lg="3" sm="8">
              <div>Test</div>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
};
