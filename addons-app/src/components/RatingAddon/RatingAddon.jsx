import React, { useEffect, useState } from "react";
import { FaStar } from "react-icons/fa";
import { updateAdonRating } from "../../services/addon-service";
import "./rating-addon.css";

export const RatingAddon = ({ addon }) => {
  const [rating, setRating] = useState(0);
  const [hover, setHover] = useState(null);

  useEffect(() => {
    setRating(addon.rating);
  }, [addon]);

  const handleRating = (currentRating) => {
    const newRating =
      (addon.rating * addon.timesRated + currentRating) /
      (addon.timesRated + 1);

    setRating(newRating);

    updateAdonRating(addon.id, newRating, addon.timesRated + 1).catch((e) =>
      console.log(e.message)
    );
  };

  return (
    <div>
      {[...Array(5)].map((star, i) => {
        const ratingValue = i + 1;

        return (
          <label key={i}>
            <input
              type="radio"
              name="rating"
              value={ratingValue}
              onClick={() => handleRating(ratingValue)}
            />
            <FaStar
              className="star"
              color={ratingValue <= (hover || rating) ? "#ffc107" : "#e4e5e9"}
              size={15}
              onMouseEnter={() => setHover(ratingValue)}
              onMouseLeave={() => setHover(null)}
            />
          </label>
        );
      })}
      <h6>Rating {rating ? rating.toFixed(2) : 0}</h6>
    </div>
  );
};
