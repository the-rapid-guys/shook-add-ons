import React from 'react'
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { registerUser } from '../../services/authentication'
import {createUserProfile, getUserByProfile} from '../../services/users-service'
import { Link } from 'react-router-dom'
import {Container, Row, Col} from 'reactstrap'
import './sign_up.css'
export const SignUpForm = () => {
  const [form, setForm] = useState({
    username:'',
    email:'',
    password:'',
    phoneNumber:'',
    createdAt: '',
  });

const navigate = useNavigate();

const handleChange = prop => e => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const register = (e) => {
    e.preventDefault();

    if(form.password.length < 6){
      return alert('Password should be at least 6 symbols')
    }
    if(form.username.length < 2 || form.username.length > 20){
      return alert('Username should be at least 2 symbols and less then 20')
    }
    if(form.phoneNumber.length < 10 || form.phoneNumber.length > 10){
      return alert ('Phone number should be 10 digits')
    }

    getUserByProfile(form.username)
    .then(snapshot => {
      if (snapshot.exists()) {
          return alert(`User with username ${form.username} already exists!`);
        }

        return registerUser(form.username, form.email, form.password,form.phoneNumber)
          .then(u => {
            createUserProfile(form.username, u.user.uid, u.user.email, form.phoneNumber)
            .then(() => {
              navigate('/home');
            })
            // .catch(console.log(e.message));
          })
          .catch(e => {
            console.log(e.message)
            if (e.message.includes(`email-already-in-use`)) {
              alert(`Email ${form.email} has already been registered!`);
            }
          });
    })
    .catch(console.log(e.message));
  }

  return (
  <section>
          <Container>
              <Row>
                  <Col lg='1' className=''>
      <div className='signup__wrapper'>
    
<div className="signup-box ">
  <h2>Sign Up</h2>
  <form>
    <div className="user-box">
      <input type="username" value={form.username} onChange={handleChange('username')}/>
      <label>Username</label>
    </div>
    <div className="user-box">
      <input type="email" value={form.email} onChange={handleChange('email')}/>
      <label>Email</label>
    </div>
    <div className="user-box">
      <input type="password" value={form.password} onChange={handleChange('password')}/>
      <label>Password</label>
    </div>
    <div className="user-box">
      <input type="phoneNumber" value={form.phoneNumber} onChange={handleChange('phoneNumber')}/>
      <label>Phone number</label>
    </div>
    <button type='submit' onClick={register}><Link to='/sign-in'>Sign Up</Link>
        {/* <span></span> */}
      
    </button>
  </form><br/>
  <p className='d-flex align-items-center justify-content-between'>
                                        Already have an account? <Link to="/sign-in" ><span>Sign in</span></Link>.
                                    </p>
</div>
</div>
</Col>
</Row>
</Container> 
      </section>                                     
  )
}
