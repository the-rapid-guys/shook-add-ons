import React from "react";
import { useState, useEffect } from "react";
import {
  fromAddonsDocument,
  getLiveAddons,
} from "../../services/addon-service";
import { AddonCard } from "../ui/AddonCard/AddonCard";

export const Addons = ({ status }) => {
  const [addons, setAddons] = useState([]);

  useEffect(() => {
    const unsubscribe = getLiveAddons((snapshot) => {
      setAddons(
        fromAddonsDocument(snapshot).filter((el) => el.status === status)
      );
    });

    return () => unsubscribe();
  });

  return (
    <>
      {addons.length > 0 &&
        addons.slice(0, 5).map((addon) => {
          return <AddonCard key={addon.id} addon={addon} />;
        })}
    </>
  );
};
