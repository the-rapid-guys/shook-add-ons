import React, { useEffect, useState } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
// import {AddonCard} from '../AddonCard/AddonCard'
import './featured.css';
import { AddonCard } from '../AddonCard/AddonCard';
import {
  fromAddonsDocument,
  getLiveAddons,
} from '../../../services/addon-service';

export const Featured = () => {
  const [addons, setAddons] = useState([]);

  useEffect(() => {
    const unsubscribe = getLiveAddons((snapshot) => {
      setAddons(fromAddonsDocument(snapshot).filter((el) => el.status === 2));
    });
    return () => unsubscribe();
  }, []);

  return (
    <section>
      <Container>
        <Row className="d-flex">
          <Col lg="12" className="mb-5">
            <div className="featured__top d-flex align-items-center justify-content-between">
              <h3>Featured</h3>
              <span>
                <Link to="/market">Explore more</Link>
              </span>
            </div>
          </Col>

          <Col lg="11">
            <div className="featured__top d-flex align-items-center ">
              {addons.length > 0 &&
                addons
                  .sort((a, b) => a.rating - b.rating)
                  .slice(0, 5)
                  .map((addon) => (
                    <AddonCard key={addon.id} addon={addon} status={2} />
                  ))}
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};
