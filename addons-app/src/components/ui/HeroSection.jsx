import React from "react";
import AppContext from "../../providers/AppContext";
import { useContext } from "react";
import { Container, Row, Col } from "reactstrap";
import { Link } from "react-router-dom";
import "./hero_section.css";
import heroImg from "../../assets/images/global-communications.png";
export const HeroSection = () => {
  const appState = useContext(AppContext);

  return (
    <section className="hero__section">
      <Container>
        <Row>
          <Col lg="6" md="6">
            <div className="hero__container">
              <h2>
                Explore, Create and Share plugins for{" "}
                <span>better dev experience</span>
              </h2>
              <p>
                SHOOK Plug-in Marketplace offers a variety of plug-ins to help
                anyone customize their application more efficiently.
              </p>
              <div className="hero__btns d-flex align-items-center gap-4">
                <button className=" explore__btn d-flex align-items-center gap-2">
                  <i className="ri-rocket-line"></i>
                  <Link to="/market">Explore</Link>
                </button>
                <button className="create__btn d-flex align-items-center gap-2">
                  <i className="ri-lightbulb-flash-line"></i>
                  {appState.user === null ? (
                    <Link to="">Create</Link>
                  ) : (
                    <Link to="/create">Create</Link>
                  )}
                </button>
              </div>
            </div>
          </Col>
          <Col lg="6" md="6">
            <div className="hero__img">
              <img src={heroImg} alt="" className="w-100"></img>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};
