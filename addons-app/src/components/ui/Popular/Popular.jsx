import { Container, Col, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import { useEffect, useState } from 'react';
import {
  fromAddonsDocument,
  getLiveAddons,
} from '../../../services/addon-service';
import { AddonCard } from '../AddonCard/AddonCard';

export const Popular = () => {
  const [addons, setAddons] = useState([]);

  useEffect(() => {
    const unsubscribe = getLiveAddons((snapshot) => {
      setAddons(fromAddonsDocument(snapshot).filter((el) => el.status === 2));
    });
    return () => unsubscribe();
  }, []);

  return (
    <section>
      <Container>
        <Row className="d-flex">
          <Col lg="12" className="mb-5">
            <div className="featured__top d-flex align-items-center justify-content-between">
              <h3>Popular</h3>
              <span>
                <Link to="/market">Explore more</Link>
              </span>
            </div>
          </Col>

          <Col lg="11">
            <div className="featured__top d-flex align-items-center">
              {addons.length > 0 &&
                addons
                  .sort((a, b) =>
                    a.downloads < b.downloads
                      ? 1
                      : a.downloads > b.downloads
                      ? -1
                      : 0
                  )
                  .slice(0, 5)
                  .map((addon) => (
                    <AddonCard key={addon.id} addon={addon} status={2} />
                  ))}
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};
