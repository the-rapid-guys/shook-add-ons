import React from "react";
import "./common_section.css";
import { Container } from "reactstrap";
export const CommonSection = ({ title }) => {
  return (
    <section className="common__section">
      <Container className="text-center">
        <h2>{title}</h2>
      </Container>
    </section>
  );
};
