import React, { useEffect } from 'react';
import { Container, Row, Col } from 'reactstrap';
import { Link } from 'react-router-dom';
import { useState } from 'react';
import { getLastAddons } from '../../../services/addon-service';
import { AddonCard } from '../AddonCard/AddonCard';

export const New = () => {
  const [addons, setAddons] = useState([]);

  useEffect(() => {
    getLastAddons().then((res) => {
      setAddons(res.filter((el) => el.status === 2));
    });
  }, []);

  return (
    <section>
      <Container>
        <Row className="d-flex">
          <Col lg="12" className="mb-5">
            <div className="featured__top d-flex align-items-center justify-content-between">
              <h3>New</h3>
              <span>
                <Link to="/market">Explore more</Link>
              </span>
            </div>
          </Col>
        </Row>
      </Container>
      <Container>
        <Row>
          <Col lg="11">
            <div className="featured__top d-flex align-items-center ">
              {addons.length > 0 &&
                addons.map((addon) => (
                  <AddonCard key={addon.id} addon={addon} status={2} />
                ))}
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};
