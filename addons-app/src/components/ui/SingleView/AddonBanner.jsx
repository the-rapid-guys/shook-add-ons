import React from "react";
import { Container } from "reactstrap";
import "./addon_banner.css";

export const AddonBanner = ({ author }) => {
  // const { user, userData } = useContext(AppContext);
  // const location = useLocation();
  return (
    <>
      <section className="addon__section">
        <Container className="text-center">
          <h2>{author}</h2>
        </Container>
      </section>
    </>
  );
};
