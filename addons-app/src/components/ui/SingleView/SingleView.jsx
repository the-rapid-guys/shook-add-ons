import React from "react";
import { AddonBanner } from "./AddonBanner";
import { Container, Row, Col } from "reactstrap";
import { RatingAddon } from "../../RatingAddon/RatingAddon";
import { useContext } from "react";
import AppContext from "../../../providers/AppContext";
import { updateStatus } from "../../../services/addon-service";
import { usersRole } from "../../../services/users-service";
import "./single_view.css";

export const SingleView = ({
  addon,
  description,
  issuesCount,
  lastCommit,
  pullsCount,
}) => {
  const { userData } = useContext(AppContext);

  const handleClick = (id, url) => {};

  return (
    <>
      <AddonBanner author={addon.title} />
      <section>
        <Container>
          <Row>
            <Col lg="3" style={{ display: "inline-grid" }}>
              <img
                src={addon.logoDownloadUrl}
                alt="logo"
                className="w-50 single__addon_img"
              />
              <RatingAddon addon={addon} />
              <div className="download md-2 d-flex align-item-center justify-content-between">
                {addon.status === 2 ? (
                  <a href={addon?.FileDownloadUrl}>
                    <button
                      onClick={() =>
                        handleClick(addon.id, addon.FileDownloadUrl)
                      }
                    >
                      Get
                    </button>
                  </a>
                ) : (
                  userData?.role === usersRole.ADMIN && (
                    <button onClick={() => updateStatus(addon.id, 2)}>
                      Approve
                    </button>
                  )
                )}
              </div>
            </Col>

            <Col lg="4">
              <div className="single__addon__content">
                <h2>{addon.title}</h2>
                <div className="d-flex align-item-center justify-content-between">
                  <div className="d-flex align-items-center gap-4">
                    <span>
                      <i
                        className="ri-download-2-line"
                        style={{ color: "white" }}
                      >
                        <br />
                        {addon.downloads}
                      </i>
                    </span>
                  </div>
                </div>
                <div className="single-addon-content">
                  <p>Created by: {addon.author}</p>
                  <p>
                    Description: <h6>{description}</h6>
                  </p>
                  <p>
                    Issues count: <h6>{issuesCount}</h6>
                  </p>
                  <p>
                    Pull requests count: <h6>{pullsCount}</h6>
                  </p>
                  <p>
                    Last commit date: <h6>{lastCommit}</h6>
                  </p>
                  <span className="origin__link">
                    <a href={addon.originURL}>GitHub link</a>
                  </span>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </>
  );
};
