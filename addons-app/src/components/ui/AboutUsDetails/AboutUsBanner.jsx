import React from "react";
import { Container } from "reactstrap";
import "./about_us_banner.css";

export const AboutUsBanner = () => {
  return (
    <>
      <section className="about__us__section">
        <Container className="text-center">
          <h2>About Us</h2>
        </Container>
      </section>
    </>
  );
};
