import React from "react";
import { Col, Container, Row } from "reactstrap";
// import { AboutUsBanner } from "./AboutUsBanner";

export const AboutUsDetails = () => {
  return (
    <>
      {/* <AboutUsBanner /> */}
      <section>
        <Container>
          <Row>
            <Col md={{ span: 6, offset: 4 }}>
              <div
                className="about__us d-flex align-items-center"
                style={{ color: "white", fontWeight: "300" }}
              >
                <h3>The Story of SHOOK Marketplace</h3>
              </div>
            </Col>
          </Row>

          <div
            className="d-flex align-items-center"
            style={{ color: "white", fontWeight: "lighter" }}
          >
            <h5>
              Founded by two big enthusiasts in 2022, SHOOK Marketplace is a new
              fresh platform for digital goods, connecting our users to amazing
              items. Based between two countries Bulgaria and Israel, the SHOOK
              platform was built for the creators, the doers, the imagineers,
              the bloggers and the entrepreneur to provide the tools that bring
              ideas to life.
            </h5>
          </div>
        </Container>
      </section>
    </>
  );
};
