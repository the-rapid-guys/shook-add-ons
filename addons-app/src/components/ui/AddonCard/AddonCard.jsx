import { Link } from "react-router-dom";
import AppContext from "../../../providers/AppContext";
import { useContext, useEffect, useState } from "react";
// import {
//   getUserRepo,
//   getLastCommit,
//   pullRequestsCount,
//   API_URL,
// } from "../../../services/github-service";
import {
  getLiveUsers,
  fromUsersDocument,
  usersRole,
} from "../../../services/users-service";
import defaultAvatar from "../../../assets/images/default_pic.png";
import "../Featured/featured.css";
import { RatingAddon } from "../../RatingAddon/RatingAddon";
import {
  deleteAddon,
  updateDownloads,
  updateStatus,
} from "../../../services/addon-service";

/**
 *
 * @param {{ addon: { id: string, title: string, author: string, createdOn: Date, category: string, tags: string[], fileUrl: string, logoUrl: string, originURL: string, status: string } } } addon
 */

export const AddonCard = ({
  addon,
  description,
  issuesCount,
  lastCommit,
  pullsCount,
}) => {
  const { userData } = useContext(AppContext);
  // const [repoInfo, setRepoInfo] = useState({
  //   description: "",
  //   issuesCount: "",
  //   lastCommit: "",
  //   pullsCount: "",
  // });
  // const [users, setUsers] = useState([]);
  const [setUsers] = useState([]);
  // const navigate = useNavigate();

  useEffect(() => {
    getLiveUsers((snapshot) => setUsers(fromUsersDocument(snapshot)));
  }); //removed dependency array

  const handleClick = (id, url, download) => {
    updateDownloads(id, download);
    console.log(1, updateDownloads(id, download));
  };

  return (
    <div className="single__addon__card justify-content-between gap-5">
      <div className="addon__img">
        <img src={addon?.logoDownloadUrl} alt="logo" className="w-50" />
      </div>

      <div className="addon__content">
        <h5 className="addon__title">
          <Link to={`/market/${addon.id}`}>{addon.title}</Link>
        </h5>
        <div className="creator__info__wrapper d-flex gap-3">
          <div className="creator__img">
            {addon.authorAvatar ? (
              <img src={addon.authorAvatar} alt="avatar" className="w-100" />
            ) : (
              <img src={defaultAvatar} alt="avatar" className="w-100" />
            )}
          </div>

          <div className="creator__info w-100 d-flex align-items-center justify-content-between">
            <div>
              <h6>Created by</h6>
              <p>{addon.author}</p>
            </div>
          </div>
          <div className="d-flex align-items-center gap-4">
            <h6>
              <i className="ri-download-2-line" style={{ color: "white" }}>
                <br />
                {addon.downloads}
              </i>
            </h6>
          </div>
        </div>
        <RatingAddon addon={addon} />
        <div className="download mt-2 d-flex align-item-center justify-content-between">
          {addon.status === 2 ? (
            <a href={addon?.FileDownloadUrl}>
              <button
                onClick={() =>
                  handleClick(
                    addon.id,
                    addon.FileDownloadUrl,
                    addon.downloads ? addon.downloads : 0
                  )
                }
              >
                Get
              </button>
            </a>
          ) : (
            userData?.role === 2 && (
              <button onClick={() => updateStatus(addon.id, 2)}>Approve</button>
            )
          )}
          {userData?.role === usersRole.ADMIN && (
            <button onClick={() => deleteAddon(addon.id, userData.username)}>
              Delete
            </button>
          )}
        </div>
      </div>
    </div>
  );
};
