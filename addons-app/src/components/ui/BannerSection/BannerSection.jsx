import React from "react";
import { Container } from "reactstrap";
import AppContext from "../../../providers/AppContext";
import { useContext } from "react";
import "./banner_section.css";

export const BannerSection = () => {
  // const { user, userData } = useContext(AppContext);
  const { userData } = useContext(AppContext);

  return (
    <>
      {userData && (
        <section className="banner__section">
          <Container className="text-center">
            <h2>{userData.username}</h2>
          </Container>
        </section>
      )}
    </>
  );
};
