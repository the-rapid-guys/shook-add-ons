import React, { useEffect, useState } from "react";
import { FaStar } from "react-icons/fa";
import { updateRating } from "../../services/users-service";
import "./rating.css";

export const Rating = ({ user }) => {
  const [rating, setRating] = useState(0);
  const [hover, setHover] = useState(null);

  useEffect(() => {
    setRating(user.rating);
  }, [user]);

  const handleRating = (currentRating) => {
    const newRating =
      (user.rating * user.timesRated + currentRating) / (user.timesRated + 1);

    setRating(newRating);

    updateRating(user.username, newRating, user.timesRated + 1).catch((e) =>
      console.log(e.message)
    );
  };

  return (
    <div>
      {[...Array(5)].map((star, i) => {
        const ratingValue = i + 1;

        return (
          <label key={i}>
            <input
              type="radio"
              name="rating"
              value={ratingValue}
              onClick={() => handleRating(ratingValue)}
            />
            <FaStar
              className="star"
              color={ratingValue <= (hover || rating) ? "#ffc107" : "#e4e5e9"}
              size={15}
              onMouseEnter={() => setHover(ratingValue)}
              onMouseLeave={() => setHover(null)}
            />
          </label>
        );
      })}
      <p>Your rating is {rating ? rating.toFixed(2) : 0}.</p>
    </div>
  );
};
