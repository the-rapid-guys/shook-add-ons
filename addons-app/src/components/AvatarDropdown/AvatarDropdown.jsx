import { Link } from 'react-router-dom';
import { useContext } from 'react';
import AppContext from '../../providers/AppContext';
import { logoutUser } from '../../services/authentication';
import defaultAvatar from '../../assets/images/default_pic.png';
import './avatar_dropdown.css';
export const AvatarDropdown = () => {
  const appState = useContext(AppContext);
  const { userData } = useContext(AppContext);

  const logout = (e) => {
    e.preventDefault();
    logoutUser().then(() => {
      appState.setContext({ user: null, userData: null });
    });
  };
  return (
    <div className="dropdown">
      <button className="btn__avatar d-flex align-items-center gap-2">
        {userData?.avatarUrl ? (
          <img src={userData?.avatarUrl} alt="user_avatar" />
        ) : (
          <img src={defaultAvatar} alt="default_avatar" />
        )}
      </button>
      <div className="dropdown__content align-item-center">
        <ul className="dropdown__ul align-item-center flex-direction-column">
          <Link to="/author-profile">
            <span>@{userData?.username}</span>
          </Link>
          <br />

          {/* <Link to="author-profile"><span>My Profile</span></Link><br/> */}
          <Link to="edit-profile">
            <span>Edit profile</span>
          </Link>
          <br />

          {/* <Link to="/home"><button className="btn__sign__out" onClick={logout}>Sign out</button></Link> */}
          {userData?.role === 2 ? (
            <Link to="/admin-board">
              <span>Admin board</span>
              <br />
            </Link>
          ) : null}

          <button className="btn__sign__out" onClick={logout}>
            <Link to="/home">
              <span>Sign out</span>
            </Link>
          </button>
        </ul>
      </div>
    </div>
  );
};
