import React from "react";
import { useState } from "react";
import {
  ref as storageRef,
  getDownloadURL,
  uploadBytes,
} from "firebase/storage";
import { storage } from "../../firebaseConfig/firebase-config";
import {
  updateUserEmail,
  updateUserProfilePicture,
  updateUserPhoneNumber,
} from "../../services/users-service";
import AppContext from "../../providers/AppContext";
import { useContext } from "react";
import { Container, Row, Col } from "reactstrap";
import "../Upload/UploadAvatar/upload_avatar.css";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

export const EditForm = () => {
  const { user, userData, setContext } = useContext(AppContext);
  const [open, setOpen] = useState(false);

  const [form, setForm] = useState({
    email: userData?.email,
    phoneNumber: userData?.phoneNumber,
    avatar: "",
  });

  const handleClick = () => {
    setOpen((prev) => !prev);
  };

  const handleEdit = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const editProfile = (e) => {
    e.preventDefault(e);

    const file = e.target[2]?.files?.[0];

    if (file) {
      const picture = storageRef(storage, `images/${userData.username}/files`);

      uploadBytes(picture, file)
        .then((snapshot) => {
          return getDownloadURL(snapshot.ref).then((url) => {
            return updateUserProfilePicture(userData.username, url).then(() => {
              setContext({
                user,
                userData: {
                  ...userData,
                  fileUrl: url,
                },
              });
            });
          });
        })
        .then((res) => {
          handleClick();
        })
        .catch(console.error);
    }

    if (form.email) {
      updateUserEmail(userData.username, form.email)
        .then((res) => {
          handleClick();
        })
        .catch(console.error);
    }

    if (form.phoneNumber) {
      updateUserPhoneNumber(userData.username, form.phoneNumber)
        .then((res) => {
          handleClick();
        })
        .catch(console.error);
    }
  };

  return (
    <section>
      <Container>
        <Row>
          <Col lg="12" className="mb-5">
            <div className="upload__wrapper">
              <div className="upload__box ">
                <h2>Edit profile</h2>
                <form onSubmit={editProfile}>
                  <div className="user__box">
                    <input
                      type="text"
                      name="email"
                      value={form.email}
                      onChange={handleEdit("email")}
                    />
                    <label>
                      <i className="ri-pencil-line"></i>Email
                    </label>
                  </div>
                  <div className="user__box">
                    <input
                      type="text"
                      name="phoneNumber"
                      value={form.phoneNumber}
                      onChange={handleEdit("phoneNumber")}
                    />
                    <label>
                      <i className="ri-pencil-line"></i>Phone number
                    </label>
                  </div>
                  {userData?.avatarUrl ? (
                    <img src={userData.avatarUrl} alt="profile" />
                  ) : null}
                  <div className="submit__form gap-3">
                    <label>
                      <i class="ri-pencil-line">Avatar</i>
                    </label>
                    <input
                      type="file"
                      name="avatar"
                      value={form.avatar}
                      onChange={handleEdit("avatar")}
                    ></input>

                    <button type="submit" className="d-flex">
                      Submit
                    </button>
                    <Snackbar
                      open={open}
                      autoHideDuration={6000}
                      onClose={handleClick}
                    >
                      <Alert
                        onClose={handleClick}
                        severity="success"
                        sx={{ width: "100%" }}
                      >
                        Succsessfully edited profile information!
                      </Alert>
                    </Snackbar>
                  </div>
                </form>

                <br />
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    </section>
  );
};
