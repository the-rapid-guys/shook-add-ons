import {
  get,
  set,
  ref,
  query,
  equalTo,
  orderByChild,
  update,
  onValue,
} from 'firebase/database';
import { db } from '../firebaseConfig/firebase-config';

export const usersRole = {
  BASIC: 1,
  ADMIN: 2,
  BLOCKED: 3,
};

export const getUserByProfile = (username) => {
  return get(ref(db, `users/${username}`));
};

export const createUserProfile = (username, uid, email, phoneNumber) => {
  return set(ref(db, `users/${username}`), {
    username,
    uid,
    email,
    phoneNumber,
    createdOn: new Date(),
    role: usersRole.BASIC,
    rating: 0,
    timesRated: 0,
  });
};
export const getUserData = (uid) => {
  return get(query(ref(db, 'users'), orderByChild('uid'), equalTo(uid)));
};

export const updateUserRole = (username, role) => {
  return update(ref(db), {
    [`users/${username}/role`]: role,
  });
};
export const updateUserProfilePicture = (username, url) => {
  return update(ref(db), {
    [`users/${username}/avatarUrl`]: url,
  });
};

export const updateUserEmail = (username, email) => {
  return update(ref(db), {
    [`users/${username}/email`]: email,

  });
};

export const updateUserPhoneNumber = (username, phoneNumber) => {
  return update(ref(db), {
    [`users/${username}/phoneNumber`]: phoneNumber,
  });
};

export const updateUserAddon = (username, addonID) => {

  return update(ref(db), {
    [`users/${username}/addons/${addonID}`]: true,
  });
};

export const getUserById = (uid) => {
  return get(ref(db, `users/${uid}`));
};

export const getLiveUsers = (listen) => {
  return onValue(ref(db, 'users'), listen);
};

export const fromUsersDocument = (snapshot) => {
  const usersDocument = snapshot.val();

  return Object.keys(usersDocument).map((key) => {
    const user = usersDocument[key];

    return {
      ...user,
      uid: key,
      email: user.email,
      username: user.username,
      phoneNumber: user.phoneNumber,
      role: user.role,
      addons: user.addons ? Object.keys(user.addons) : [],

    };
  });
};

export const userBlocking = (username, role) => {
  return update(ref(db), {
    [`users/${username}/role`]: role,
  });
};

export const updateRating = (username, newRating, timesRated) => {
  return update(ref(db), {
    [`users/${username}/rating`]: newRating,
    [`users/${username}/timesRated`]: timesRated,
  });
};
