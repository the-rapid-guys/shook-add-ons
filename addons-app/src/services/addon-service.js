import {
  ref,
  push,
  get,
  query,
  equalTo,
  orderByChild,
  update,
  onValue,
} from 'firebase/database';
import { db } from '../firebaseConfig/firebase-config';
import { limitToLast } from 'firebase/database';

export const addonStatus = {
  PENDING: 1,
  APPROVED: 2,
  REJECTED: 3,
};

export const fromAddonsDocument = (snapshot) => {
  const addonDocument = snapshot.val();
  return Object.keys(addonDocument)
    .reverse()
    .map((key) => {
      const addon = addonDocument[key];

      return {
        ...addon,
        id: key,
        createdAt: new Date(addon.createdAt),
        // downloads: addon.downloads[0],
        // rating: addon.rating[0],
        // status: addonStatus.PENDING, //state: pending, approved, reject
      };
    });
};

export const createAddon = (
  title,
  originURL,
  category,
  tags,
  username,
  logoUrl,
  fileUrl,
  avatarUrl
) => {
  return push(ref(db, 'addons'), {
    title,
    originURL,
    category,
    tags,
    author: username,
    logoUrl,
    fileUrl,
    authorAvatar: avatarUrl,
    createdAt: Date.now(),
    status: addonStatus.PENDING,
    rating: 0,
    timesRated: 0,
  }).then((result) => {
    return { id: result.key };
    // return getAddonById(result.key);
  });
};

export const getAddonById = (id) => {
  return get(ref(db, `addons/${id}`)).then((result) => {
    if (!result.exists()) {
      throw new Error(`PlugIn with id ${id} does not exist!`);
    }
    const addon = result.val();
    addon.id = id;
    addon.createdAt = new Date(addon.createdAt);

    return addon;
  });
};

export const getAddonByAuthor = (username) => {
  return get(
    query(ref(db, 'addons'), orderByChild('author'), equalTo(username))
  ).then((snapshot) => {
    if (!snapshot.exists()) return [];
    console.log(Object.keys(snapshot.val()));

    return fromAddonsDocument(snapshot);
  });
};

export const getLastAddons = () => {
  return get(query(ref(db, 'addons'), limitToLast(5))).then((snapshot) => {
    if (!snapshot.exists()) return [];
    // console.log(Object.keys(snapshot.val()));

    return fromAddonsDocument(snapshot);
  });
};

export const getLiveAddons = (listen) => {
  return onValue(ref(db, 'addons'), listen);
};

export const deleteAddon = (id, username) => {
  return update(ref(db), {
    [`/addons/${id}`]: null,
    [`/users/${username}/addons/${id}`]: null,
  });
};

export const updateAddonDownloadLink = (id, url) => {
  return update(ref(db), {
    [`addons/${id}/FileDownloadUrl`]: url,
  });
};

export const updateAddonDownloadLinkLogo = (id, url) => {
  return update(ref(db), {
    [`addons/${id}/logoDownloadUrl`]: url,
  });
};

export const updateAdonRating = (id, newRating, timesRated) => {
  return update(ref(db), {
    [`addons/${id}/rating`]: newRating,
    [`addons/${id}/timesRated`]: timesRated,
  });
};

export const updateStatus = (id, status) => {
  return update(ref(db), {
    [`addons/${id}/status`]: status,
  });
};

export const updateDownloads = (addon, download) => {
  let numOfDownloads = download + 1;
  return update(ref(db), {
    [`addons/${addon}/downloads`]: numOfDownloads,
  });
};
