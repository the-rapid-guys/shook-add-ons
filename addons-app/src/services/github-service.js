import axios from 'axios'

export const API_URL = 'https://api.github.com/repos/';
export const getUserRepo = (subdirectory) => {
  return axios.get(API_URL + subdirectory)
    .then(res => {
      const repoData = res.data;

      return repoData;
    })
    .catch(err => {
      console.log(err)
      return {
        description: 'N/A',
        open_issues_count: 'N/A'
      }
    });
}

export const getLastCommit = (subdirectory) => {
  return axios.get(API_URL + subdirectory + '/commits')
    .then(res => {
      const lastCommitDate = (res.data[0].commit.author.date).split('T')[0].split('-').join('/');

      return lastCommitDate;
    })
    .catch(err => {
      console.log(err)
      return 'N/A';
    })
}

export const pullRequestsCount = (subdirectory) => {
  return axios.get(API_URL + subdirectory + '/pulls')
    .then((res) => {
      const pulls = res.data[0] ? res.data[0].number : 0;

      return pulls;
    })
    .catch(err => {
      console.log(err)
      return 'N/A'
    })
}