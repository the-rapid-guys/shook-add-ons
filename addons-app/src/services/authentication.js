import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut,
} from 'firebase/auth';
import { auth } from '../firebaseConfig/firebase-config';

export const registerUser = (username, email, password, phoneNumber,) => {
  return createUserWithEmailAndPassword(
    auth,
    email,
    password,
    username,
    phoneNumber,
  );
};

export const loginUser = (email, password) => {
  return signInWithEmailAndPassword(auth, email, password);
};

export const logoutUser = () => {
  return signOut(auth);
};
