import { Routes, Route, Navigate } from 'react-router-dom';
import { Home } from '../pages/Home';
import { Market } from '../pages/Market';
import { EditProfile } from '../pages/EditProfile';
import { Create } from '../pages/Create';
import { AuthorProfile } from '../pages/AuthorProfile';
import { Contact } from '../pages/Contact';
import { AddonDetails } from '../pages/AddonDetails';
import { SignUp } from '../pages/SignUp';
import { SignIn } from '../pages/SignIn';
import { AdminBoard } from '../pages/AdminBoard';
import { ProfileViewAdmin } from '../components/ProfileDetails/ProfileViewAdmin';
import { About } from '../pages/About';

export const Routers = () => {
  return (
    <Routes>
      <Route path="/" element={<Navigate to="/home" />} />
      <Route path="/home" element={<Home />} />
      <Route path="/sign-in" element={<SignIn />} />
      <Route path="/sign-up" element={<SignUp />} />
      <Route path="/about" element={<About />} />
      <Route path="/author-profile" element={<AuthorProfile />} />
      <Route exact path="/market" element={<Market />} />
      <Route path="/create" element={<Create />} />
      <Route path="/market/:id/" element={<AddonDetails />} />
      <Route path="/contact" element={<Contact />} />
      <Route path="/edit-profile" element={<EditProfile />} />
      <Route path="/admin-board" element={<AdminBoard />} />
      <Route path="/admin-board-profile" element={<ProfileViewAdmin />} />
    </Routes>
  );
};
