import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { getStorage } from 'firebase/storage'

const firebaseConfig = {
  apiKey: 'AIzaSyDOVkZOjqKmGTkUdmNVzR3GAcYxzgr_4vA',
  authDomain: 'addons-project-c4005.firebaseapp.com',
  projectId: 'addons-project-c4005',
  storageBucket: 'gs://addons-project-c4005.appspot.com',
  messagingSenderId: '279940217980',
  appId: '1:279940217980:web:c747ba0caf62bf5f901df9',
  measurementId: 'G-R7LLV0SLYB',
  databaseURL:
    'https://addons-project-c4005-default-rtdb.europe-west1.firebasedatabase.app/',
};

export const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);

export const db = getDatabase(app);

export const storage = getStorage(app);