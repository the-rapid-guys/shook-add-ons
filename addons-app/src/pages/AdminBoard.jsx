import { AdminDashboard } from '../components/AdminDashboard/AdminDashboard';

export const AdminBoard = () => {
  return (
    <>
      <AdminDashboard />
    </>
  );
};
