import { ProfileDetails } from "../components/ProfileDetails/ProfileDetails"
import { BannerSection } from "../components/ui/BannerSection/BannerSection"

export const AuthorProfile = () => {
  return (
    <>
      <BannerSection/>
      <ProfileDetails/>
    </>
  )
}