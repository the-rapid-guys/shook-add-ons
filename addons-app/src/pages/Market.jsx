import { CommonSection } from "../components/ui/CommonSection/CommonSection";
import { CategoriesSort } from "../components/CategoriesSort/CategoriesSort";
export const Market = () => {
  // const handleFilter = ()=>{

  // }

  // const handleCategory = ()=>{

  // }

  // const handleSort = ()=>{

  // }

  return (
    <>
      <CommonSection title={"MarketPlace"} />
      <CategoriesSort />
    </>
  );
};
