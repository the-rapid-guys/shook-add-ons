import { HeroSection } from "../components/ui/HeroSection";
import { Featured } from "../components/ui/Featured/Featured";
import { New } from "../components/ui/NewAddons/New";
import { Popular } from "../components/ui/Popular/Popular";

export const Home = () => {
  return (
    <>
      <HeroSection />
      <Featured />
      <New />
      <Popular />
    </>
  );
};
