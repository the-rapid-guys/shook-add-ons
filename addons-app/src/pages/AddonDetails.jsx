import { SingleView } from "../components/ui/SingleView/SingleView";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
// import AppContext from "../providers/AppContext";
import { getAddonById } from "../services/addon-service";
import {
  getLastCommit,
  getUserRepo,
  pullRequestsCount,
} from "../services/github-service";

export const AddonDetails = () => {
  // const { userData } = useContext(AppContext);
  const [addon, setAddon] = useState({});
  let { id } = useParams();

  const [repoInfo, setRepoInfo] = useState({
    description: "",
    issuesCount: "",
    lastCommit: "",
    pullsCount: "",
  });

  const getSubDirectory = addon.originURL?.split("/").slice(3, 5).join("/");

  useEffect(() => {
    getAddonById(id)
      .then((res) => setAddon(res))
      .catch((err) => console.log(err));
  }, [id]);

  useEffect(() => {
    if (getSubDirectory) {
      Promise.all([
        getUserRepo(getSubDirectory),
        getLastCommit(getSubDirectory),
        pullRequestsCount(getSubDirectory),
      ]).then(([res, commit, pulls]) => {
        setRepoInfo((prev) => ({
          ...prev,
          description: res.description,
          issuesCount: res.open_issues_count,
          lastCommit: commit,
          pullsCount: pulls,
        }));
      });
      // (async () => {
      //   try {
      //     const res = await getUserRepo(getSubDirectory);
      //     const commit = await getLastCommit(getSubDirectory);
      //     const pulls = await pullRequestsCount(getSubDirectory);
      //     setRepoInfo((prev) => ({
      //       ...prev,
      //       description: res.description,
      //       issuesCount: res.open_issues_count,
      //       lastCommit: commit,
      //       pullsCount: pulls,
      //     }));
      //   } catch (err) {
      //     console.log(err);
      //     return;
      //   }
      // })();
    }
  }, [getSubDirectory]);

  return (
    <div>
      <SingleView
        id={id}
        addon={addon}
        description={repoInfo.description}
        issuesCount={repoInfo.issuesCount}
        lastCommit={repoInfo.lastCommit}
        pullsCount={repoInfo.pullsCount}
      />
    </div>
  );
};
