import React from "react";
import { AboutUsBanner } from "../components/ui/AboutUsDetails/AboutUsBanner";
import { AboutUsDetails } from "../components/ui/AboutUsDetails/AboutUsDetails";

export const About = () => {
  return (
    <>
      <AboutUsBanner />
      <AboutUsDetails />
    </>
  );
};
