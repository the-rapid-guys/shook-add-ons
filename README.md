# **Shook Addons**

<img src="./addons-app/src/assets/img_ReadMe/shook-add-ons.jpeg"/>

Open app in a browser here: https://the-rapid-guys.gitlab.io/shook-add-ons

## 1. Description:

### Shook Addons is a SPA based on IDE Addon that allows users to do the following features:


#### **Public part of the app is accessible without authentication:**

- See the details of the Addons(name, Description, Creator, Rating etc.)

<img src="./addons-app/src/assets/img_ReadMe/addon_details.jpeg" width="260" height="190"/>
<p>&nbsp;</p>

- Use the sort/filter functionality in "MarketPlace" section

<img src="./addons-app/src/assets/img_ReadMe/marketplace.jpeg" width="600" height="190"/>
<p>&nbsp;</p>

- Download any addon via "Get" button

<img src="./addons-app/src/assets/img_ReadMe/download.jpeg" width="300" height="200"/>
<p>&nbsp;</p>

- Anonymous  users can register and login 

<img src="./addons-app/src/assets/img_ReadMe/signUp.jpeg" width="200" height="300"/>
<p>&nbsp;</p>

#### **Private part accessible only if the user is authenticated. Users:**

- Can login/logout

<img src="./addons-app/src/assets/img_ReadMe/signIn.jpeg" width="300" height="350"/>
<p>&nbsp;</p>
- Can view their personal information/addons and edit mail and phone
<p>&nbsp;</p>
<img src="./addons-app/src/assets/img_ReadMe/personal_info.jpeg" width="320" height="350"/>
<img src="./addons-app/src/assets/img_ReadMe/edit_profile.jpeg" width="280" height="350"/>
<p>&nbsp;</p>

- Can rate addons by stars under the addon logo

<img src="./addons-app/src/assets/img_ReadMe/rate.jpeg" width="300" height="400"/>
<p>&nbsp;</p>
- Can create addon in "pending" state until the Admin approves it
- The addon is visible in the public part only if approved

#### **Administrative part. The admins can:**

- Approve new addons created by the users
- See full list of users and search for any user
- Block/unblock option
- Rate any user
- Delete any addon

**Blocked User can't create addon, but all of the public part of the app is visible**


## 2. Project information:
 - The app is made by using React JS and the GitHub API.


## 3. Tools Used:
- GitHub API
- Node.js
- HTML, CSS, JavaScript, React, reactstrap, axios, Firebase



## 4. How to use the app:

Clone the repo and do an npm install. This will install all needed dependencies.

Enjoy!
